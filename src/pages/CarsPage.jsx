import React from 'react'
import Footer from '../components/footer/Footer'
import Jumbotron from '../components/jumbotron/Jumbotron'
import Navbar from '../components/navbar/Navbar'
import SearchCar from '../components/searchcar/SearchCar'

function CarsPage() {
  return (
    <div>
      <Navbar/>
      <Jumbotron/>
      <SearchCar/>
      <Footer/>
    </div>
  )
}

export default CarsPage
