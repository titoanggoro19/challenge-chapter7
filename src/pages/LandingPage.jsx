import React from "react";
import Carousel from "../components/carousel/Carousel";
import Faq from "../components/faq/Faq";
import Footer from "../components/footer/Footer";
import Jumbotron from "../components/jumbotron/Jumbotron";
import Navbar from "../components/navbar/Navbar";
import OurService from "../components/ourservice/OurService";
import WhyUs from "../components/whyus/WhyUs";
import Banner from "../components/banner/Banner";

const LandingPage = () => {
  return (
    <div>
      <Navbar/>
      <Jumbotron />
      <OurService />
      <WhyUs />
      <Carousel/>
      <Banner/>
      <Faq />
      <Footer />
    </div>
  );
};

export default LandingPage;
