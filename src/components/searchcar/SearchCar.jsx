import React from 'react'

const SearchCar = () => {
  return (
    <div>
      <div className="container" style={{ marginTop: "-80px" }}>
        <div
          id="searchColumn"
          className="card-search card p-2"
          style={{ zIndex: 2 }}
          onclick="activeDarkBackground()"
        >
          <div className="row">
            <div className="form-box search-bar">
              <div className="col mb-3">
                <label style={{ fontSize: "small" }}>Tipe Driver</label>
                <select
                  className="form-select"
                  aria-label="Default select example"
                >
                  <option selected>Pilih Tipe Driver</option>
                  <option value>Dengan Sopir</option>
                  <option value>Tanpa Sopir (Lepas Kunci)</option>
                </select>
              </div>
              <div className="col mb-3">
                <label style={{ fontSize: "small" }}>Tanggal</label>
                <input
                  placeholder="Pilih Waktu"
                  type="date"
                  id="date"
                  className="form-control"
                />
              </div>
              <div className="col mb-3">
                <label style={{ fontSize: "small" }}>Waktu Jemput/Ambil</label>
                <input
                  type="time"
                  placeholder="Pilih Waktu"
                  id="time"
                  className="form-control"
                />
              </div>
              <div className="col mb-3">
                <label style={{ fontSize: "small" }}>
                  Jumlah Penumpang(optional)
                </label>
                <input
                  type="text"
                  placeholder="Pilih Penumpang"
                  id="penumpang"
                  className="form-control"
                />
              </div>
              <div className="col">
                <button id="submitBtn" className="btn-rent-car">
                  Cari Mobil
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SearchCar
