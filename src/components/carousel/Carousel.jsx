import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

import CardCarousel from "./CardCarousel";

const options = {
  items: 4,
};
class Carousel extends React.Component {
  render() {
    return (
      <>
        <section className="testimony-section" id="testimonial-section">
          <div className="container">
            <h1>Testimonial</h1>
            <p>Berbagai review positif dari para pelanggan kami</p>
          </div>
        </section>
        <OwlCarousel
          className="owl-theme"
          loop={true}
          margin={10}
          autoHeight={true}
          nav={true}
          center={true}
          dots={false}
          navText={[
            "<img src='img/Left button.png' alt='kanan' style='height:32px; width:32px; '>",
            "<img src='img/Right button.png' alt='kanan' style='height:32px; width:32px; '>",
          ]}
          responsive={{
            0: {
              items: 1,
            },
            600: {
              items: 1,
            },
            1000: {
              items: 2,
            },
          }}
        >
          <CardCarousel />
          <CardCarousel />
          <CardCarousel />
        </OwlCarousel>
      </>
    );
  }
}

export default Carousel;
