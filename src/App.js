import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layouts from "./Layouts/Layouts";
import LandingPage from "./pages/LandingPage";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";

import "./App.css";
import CarsPage from "./pages/CarsPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layouts />} />
        <Route index path="/" element={<LandingPage />} />
        <Route index path="/cars" element={<CarsPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
